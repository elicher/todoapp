import './App.css';
import { useState, useEffect } from 'react';
import './index.css';

function App() {

const [todo, setTodo] = useState({key:'',done:false})
  
const [todos, setTodos] = useState([])

const handleInput = e => {
  e.preventDefault()
  setTodo({key:e.target.value, done:false})
  console.log({ todo })
}
const handleData = e => {
  e.preventDefault()
  console.log({ todos })
  setTodos(todos.concat(todo))
  setTodo({key:'',done:false})
}

const deleteTodo = e => {
      e.preventDefault()
      let value = e.target.value
      let newTodos = todos.filter((item) => item.key !== value);
      setTodos(newTodos)
      console.log(todos)
      console.log(e.target.value)
}

const strikeThrough = (e,i) => {
  let tempTodos = [...todos]
  tempTodos[i].done=!tempTodos[i].done
  setTodo(tempTodos)
}

const style = (item) => {
  let key = item.key
  var check = (todos.findIndex(el => el.key === item.key))
  console.log("this is"+todos[check].done)
  if (todos[check].done==true) {
    return true
  } else {
    return false
  }
}


const renderTodos = () => {
  return todos.map((item,index)=>{
    return (
      <div className="row"><h2 value={item.key} className={( (style(item)==true) ? 'strike' : 'buba')}>{item.key}</h2> 
      <div className="iconwrap"><button className="icons" onClick={deleteTodo} value={item.key}>🗑</button>
      <button className="icons" value={item.key} onClick={(event,i)=>strikeThrough(event,index)}>✅</button></div>
      </div>
  )
  })
  }

  useEffect(() => {
    console.log('after rendering')
  });

  return (
    <div className="App">
      <header className="App-header">
      <form onSubmit={handleData}>
        <div className="searchbox-wrap">
        <input className="inputField" value={todo.key} onChange={handleInput}></input>
      <button className="submit">SUBMIT</button>
      </div>
      </form>
      {renderTodos()}
      </header>
    </div>
  );
}

export default App;

